﻿using UnityEngine;
using System.Collections;

// shows how to use control change values (analog stuff). hardcoded indices but whatevs
public class TestControlChange : MonoBehaviour {
	
	void Update()
	{
		transform.localScale = Vector3.one + new Vector3(
			ValueListener.GetControlValue(2) * 10,
			ValueListener.GetControlValue(3) * 10,
			ValueListener.GetControlValue(4) * 10);
		
		transform.localEulerAngles = new Vector3(
			ValueListener.GetControlValue(14) * 360,
			ValueListener.GetControlValue(15) * 360,
			ValueListener.GetControlValue(16) * 360);
	}
}
