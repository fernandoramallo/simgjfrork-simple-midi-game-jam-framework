﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
 * Basic functions that get called whenever there's changes in the controller. Use this!
 */
public class ValueListener {
	
	// GETTERS
	// safe getters, use these to get the values of controls
	public static float GetControlValue(int Index) { if (control_values.ContainsKey(Index)) { return control_values[Index]; } else { return 0; } }
	public static bool GetButtonValue(int Index) { if (button_values.ContainsKey(Index)) { return button_values[Index]; } else { return false; } }
	
	// LISTENERS
	public static void OnNoteOn(int Index)
	{
		Debug.Log("NoteOn: note index " + Index);
		
		// do stuff here maybe
	}
	public static void OnNoteOff(int Index)
	{
		Debug.Log("NoteOff: index " + Index);
		
		// do stuff here maybe
	}
	public static void OnControlChange(int Index)
	{
		//Debug.Log("ControlChange: index " + Index + ", value " + GetControlValue(Index));
		
		// do stuff here maybe
	}
	
	
	
	
	///// PRIVATE-Y STUFF
	
	// stores the values from 0 to 1, by control index. WARNING: To get values use GetControlValue and GetButtonValue
	public static Dictionary<int, float> control_values = new Dictionary<int, float>();
	public static Dictionary<int, bool> button_values = new Dictionary<int, bool>();
	
	
}
