using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Midi;
using System;

// MIDI controller support, windows only
public class ControllerMIDI : MonoBehaviour {
	
	// the name of the device to be used (can be a section of the name, e.g. 'nanokontrol', 'launchpad')
	public string deviceName = "nanokontrol";
	
	// private
	private InputDevice ind;
	
	private bool _midienabled = true;
	public bool midienabled { get { return _midienabled; } }
	
	/// MIDI
	private List<int> _dirtyControlChanges; // which callbacks must we call by index
	private List<int> _dirtyNoteOns; // which callbacks must we call by index
	private List<int> _dirtyNoteOffs; // which callbacks must we call by index
	
	void Awake () {
		
		// WORK ON WINDOWS ONLY
		enabled = Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor;
		
		if (!enabled) {
			Debug.Log("Disabling MIDI support (works on Windows only)");
			return;
		}
		
		_dirtyControlChanges = new List<int>();
		_dirtyNoteOffs = new List<int>();
		_dirtyNoteOns = new List<int>();
		
		// MIDI
		
		if (InputDevice.InstalledDevices.Count == 0)
		{
			Debug.Log(">>> WARNING! No input devices.");
		}
		else
		{
			foreach ( InputDevice id in InputDevice.InstalledDevices )
			{
				if (id.Name.ToLower().Contains(deviceName))
					SetInputDevice(id);
			}
			
			string s = "";
			s += "Midi device IN: " + (ind == null ? "NOT FOUND" : ind.Name) + "\n";
			s += "Midi devices in:\n";
			foreach ( InputDevice id in InputDevice.InstalledDevices )
			{
				s += "- " + id.Name + "\n";
			}
			Debug.Log(s);
		}
	}
	
	// called from editor
	public void SetInputDevice(InputDevice Id)
	{
		if (!_midienabled) return;
		if (ind != null)
		{
			// close previous
			ind.RemoveAllEventHandlers();
			if (ind.IsReceiving) ind.StopReceiving();
			if (ind.IsOpen) ind.Close();
		}
		ind = Id;
		Debug.Log("Now using input device '" + ind.Name + "'");
		// event handlers
		try {
			ind.Open();
			ind.StartReceiving(null);
			ind.NoteOn += new InputDevice.NoteOnHandler(this.OnNoteOn);
			ind.NoteOff += new InputDevice.NoteOffHandler(this.OnNoteOff);
			ind.ControlChange += new InputDevice.ControlChangeHandler(this.OnControlChange);
		} catch (System.Exception e) {
			Debug.LogWarning(e);
		}
	}
	
	void Update () {
		// process stuff that changed, so it's called in the main thread like Unity likes
		int i;
		for (i = 0; i < _dirtyControlChanges.Count; i++) { ValueListener.OnControlChange(_dirtyControlChanges[i]); };
		for (i = 0; i < _dirtyNoteOns.Count; i++) { ValueListener.OnNoteOn(_dirtyNoteOns[i]); };
		for (i = 0; i < _dirtyNoteOffs.Count; i++) { ValueListener.OnNoteOff(_dirtyNoteOffs[i]); };
		_dirtyControlChanges.Clear();
		_dirtyNoteOns.Clear();
		_dirtyNoteOffs.Clear();
	}
	
	void Destroy()
	{
		OnDisable();
	}
	void OnEnable() {
		if (ind != null)
		{
			if (!ind.IsOpen) ind.Open();
			if (ind.IsOpen && !ind.IsReceiving) ind.StartReceiving(null);
		}
	}
	void OnDisable() {
		if (ind != null)
		{
			if (ind.IsOpen && ind.IsReceiving) ind.StopReceiving();
			if (ind.IsOpen) ind.Close();
		}
	}
	
	
	// MIDI listeners (do not use for coding stuff as it uses a separate thread)
	public void OnNoteOn(NoteOnMessage msg)
	{
		int index = (int)msg.Pitch;
		ValueListener.button_values[index] = true;
		_dirtyNoteOns.Add(index);
	}
	public void OnNoteOff(NoteOffMessage msg)
	{
		int index = (int)msg.Pitch;
		ValueListener.button_values[index] = false;
		_dirtyNoteOffs.Add(index);
	}
	public void OnControlChange(ControlChangeMessage msg)
	{
		float val = (msg.Value/127.0f);
		int index = (int)msg.Control;
		ValueListener.control_values[index] = val;
		_dirtyControlChanges.Add(index);
	}
	
}
